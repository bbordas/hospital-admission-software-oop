﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalAdmissionSoftwareOOP
{
    public class PeopleList
    {
        public List<Patient> patient;
        public List<Staff> staff;

        public PeopleList()
        {
            patient = new List<Patient>();
            staff = new List<Staff>();
        }

        //ADD PATIENT

        public string AddPatient(int id, string title, string firstname, string lastname, string dob, string phone, string email)
        {
            Patient p = new Patient();

            bool flag = true;
            Patient pat = new Patient();

            foreach (Patient c in patient)
            {
                if (c.PatientID == id)
                {
                    flag = false;
                    return "ID Exists in the list";
                }
            }
            if (flag)
            {
                p.PatientID = id;
                p.Title = title;
                p.FirstName = firstname;
                p.Surname = lastname;
                p.DOB = dob;
                p.Phone = phone;
                p.Email = email;


                patient.Add(p);
            }
            return title + " " + firstname + " " + lastname + " added to the Patient list";
        }


        //ADD STAFF

        public string AddStaff(int id, string title, string firstname, string lastname, string dob, string phone, string email)
        {
            bool flag = true;
            Staff s = new Staff();

            foreach (Staff c in staff)
            {
                if (c.StaffID == id)
                {
                    flag = false;
                    return "ID Exists in the list";
                }
            }
            if (flag)
            {
                s.Title = title;
                s.FirstName = firstname;
                s.Surname = lastname;
                s.DOB = dob;
                s.Phone = phone;
                s.Email = email;
                s.StaffID = id;

                staff.Add(s);
            }
            return title + " " + firstname + " " + lastname + " added to the Staff list";
        }

        //SEARCH PATIENT

        public string DisplayMatchPatient(int id)
        {
            foreach (Patient c in patient)
            {
                if (c.PatientID == id)
                {
                    return c.GetAllPatients();
                }
            }
            return "No match found";
        }


        //SEARCH STAFF

        public string DisplayMatchStaff(int id)
        {
            foreach (Staff c in staff)
            {
                if (c.StaffID == id)
                {
                    return c.GetAllStaff();
                }
            }
            return "No match found";
        }


        //DELETE STAFF

        public string DeleteStaff(int id)
        {
            for (int i = 0; i < staff.Count; i++)
            {
                Staff s = staff[i];

                if (s.StaffID == id)
                {
                    DialogResult dr = MessageBox.Show("Delete " + s.ToString(), "delete", MessageBoxButtons.OKCancel);

                    if (dr == DialogResult.OK)
                    {
                        staff.Remove(s);
                        return "Staff deleted";
                    }
                }
            }

            return "No match found";
        }



        //DELETE PATIENT

        public string DeletePatient(int id)
        {
            for (int i = 0; i < patient.Count; i++)
            {
                Patient p = patient[i];

                if (p.PatientID == id)
                {
                    DialogResult dr = MessageBox.Show("Delete " + p.ToString(), "delete", MessageBoxButtons.OKCancel);

                    if (dr == DialogResult.OK)
                    {
                        patient.Remove(p);
                        return "Patient deleted";
                    }
                }
            }

            return "No match found";
        }
    }
}
