﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalAdmissionSoftwareOOP
{
    public partial class Form1 : Form
    {
        PeopleList p = new PeopleList();

        public Form1()
        {
            InitializeComponent();
        }

        //ADD Patient and Staff
        private void btnAddPatient_Click(object sender, EventArgs e)
        {
            try
            {
                string title = cmbPatientTitle.SelectedItem.ToString();
                string firstname = txtPatientFirstName.Text;
                string lastname = txtPatientLastName.Text;
                string dob = dtpPatientDOB.Text;
                int id = int.Parse(txtPatientID.Text);
                string email = txtPatientEmail.Text;
                string phone = txtPatientPhone.Text;

                lblDisplay.Text = p.AddPatient(id, title, firstname, lastname, dob, phone, email);
            }
            catch
            {
                MessageBox.Show("Please try it again");
            }

            txtPatientFirstName.Text = " ";
            txtPatientLastName.Text = " ";
            txtPatientPhone.Text = " ";
            txtPatientEmail.Text = " ";
            txtPatientID.Text = " ";
        }

        private void btnAddStaff_Click(object sender, EventArgs e)
        {
            try
            {
                string title = cmbStaffTitle.SelectedItem.ToString();
                string firstname = txtStaffFirstName.Text;
                string lastname = txtStaffLastName.Text;
                string dob = dtpStaffDOB.Text;
                int id = int.Parse(txtStaffID.Text);
                string email = txtStaffEmail.Text;
                string phone = txtStaffPhone.Text;


                lblDisplay.Text = p.AddStaff(id, title, firstname, lastname, dob, phone, email);
            }
            catch
            {
                MessageBox.Show("Please try it again");
            }

            txtStaffFirstName.Text = " ";
            txtStaffLastName.Text = " ";
            txtStaffPhone.Text = " ";
            txtStaffEmail.Text = " ";
            txtStaffID.Text = " ";
        }

        private void btnShowPatient_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();

            foreach (Patient per in p.patient)
            {
                lstDisplay.Items.Add(per.GetAllPatients());
            }
            ConvertListToDataTable(p.patient);
        }

        void ConvertListToDataTable(List<Patient> patient)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = patient;
            dgv.DataSource = bs;
        }

        private void btnShowStaff_Click(object sender, EventArgs e)
        {
             lstDisplay.Items.Clear();

            foreach (Staff per in p.staff)
            {
                lstDisplay.Items.Add(per.GetAllStaff());
            }
            ConvertListToDataTable(p.staff);
        }

        void ConvertListToDataTable(List<Staff> staff)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = staff;
            dgv.DataSource = bs;
        }

        private void btnPatientNames_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();
            foreach (Patient per in p.patient)
            {
                lstDisplay.Items.Add(per.ToString());
            }
        }

        private void btnStaffNames_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();

            foreach (Staff per in p.staff)
            {
                lstDisplay.Items.Add(per.ToString());
            }
        }

        private void btnPatientContact_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();
            foreach (Patient per in p.patient)
            {
                lstDisplay.Items.Add(per.GetContacts());
            }
        }

        private void btnStaffContact_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();
            foreach (Staff per in p.staff)
            {
                lstDisplay.Items.Add(per.GetContacts());
            }
        }

        private void btnDeletePatient_Click(object sender, EventArgs e)
        {
            try
            {
                int removeid = int.Parse(txtShearchPatientID.Text);
                lblDisplay.Text = p.DeletePatient(removeid);
                dgv.DataSource = p.patient;

            }
            catch
            {
                MessageBox.Show("You must enter an ID");
            }
        }

        private void btnDeleteStaff_Click(object sender, EventArgs e)
        {
            try
            {
                int removestaffid = int.Parse(txtSearchStaffID.Text);
                lblDisplay.Text = p.DeleteStaff(removestaffid);
                dgv.DataSource = p.staff;


            }
            catch
            {
                MessageBox.Show("You must enter an ID");
            }
        }

        private void btnSearchPatient_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();

            try
            {
                foreach (Patient per in p.patient)
                {
                    int search = int.Parse(txtShearchPatientID.Text);
                    lblDisplay.Text = p.DisplayMatchPatient(search);
                }
            }
            catch
            {
                MessageBox.Show("Please enter a valid ID");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            lstDisplay.Items.Clear();
            try
            {
                foreach (Staff per in p.staff)
                {
                    int search = int.Parse(txtSearchStaffID.Text);
                    lblDisplay.Text = p.DisplayMatchStaff(search);
                }

            }
            catch
            {
                MessageBox.Show("Please enter a valid ID");
            }
        }
    }
}
