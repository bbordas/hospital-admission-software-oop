﻿namespace HospitalAdmissionSoftwareOOP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHospital = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.lstDisplay = new System.Windows.Forms.ListBox();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteStaff = new System.Windows.Forms.Button();
            this.txtSearchStaffID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStaffNames = new System.Windows.Forms.Button();
            this.btnStaffContact = new System.Windows.Forms.Button();
            this.btnShowStaff = new System.Windows.Forms.Button();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.dtpStaffDOB = new System.Windows.Forms.DateTimePicker();
            this.txtStaffPhone = new System.Windows.Forms.TextBox();
            this.txtStaffEmail = new System.Windows.Forms.TextBox();
            this.txtStaffID = new System.Windows.Forms.TextBox();
            this.txtStaffLastName = new System.Windows.Forms.TextBox();
            this.txtStaffFirstName = new System.Windows.Forms.TextBox();
            this.cmbStaffTitle = new System.Windows.Forms.ComboBox();
            this.lblStaffPhone = new System.Windows.Forms.Label();
            this.lblStaffEmail = new System.Windows.Forms.Label();
            this.lblStaffID = new System.Windows.Forms.Label();
            this.lblStaffDOB = new System.Windows.Forms.Label();
            this.lblStaffTitle = new System.Windows.Forms.Label();
            this.lblStaffLastName = new System.Windows.Forms.Label();
            this.lblStaffFirstName = new System.Windows.Forms.Label();
            this.cmbPatientTitle = new System.Windows.Forms.ComboBox();
            this.lblPatientPhone = new System.Windows.Forms.Label();
            this.lblPatientEmail = new System.Windows.Forms.Label();
            this.lblPatientID = new System.Windows.Forms.Label();
            this.lblPatientDOB = new System.Windows.Forms.Label();
            this.lblPatientTitle = new System.Windows.Forms.Label();
            this.lblPatientLastName = new System.Windows.Forms.Label();
            this.lblPatientFirstName = new System.Windows.Forms.Label();
            this.grbAddPatient = new System.Windows.Forms.GroupBox();
            this.btnSearchPatient = new System.Windows.Forms.Button();
            this.btnDeletePatient = new System.Windows.Forms.Button();
            this.txtShearchPatientID = new System.Windows.Forms.TextBox();
            this.lblShearchPatient = new System.Windows.Forms.Label();
            this.btnPatientNames = new System.Windows.Forms.Button();
            this.btnPatientContact = new System.Windows.Forms.Button();
            this.btnShowPatient = new System.Windows.Forms.Button();
            this.btnAddPatient = new System.Windows.Forms.Button();
            this.dtpPatientDOB = new System.Windows.Forms.DateTimePicker();
            this.txtPatientPhone = new System.Windows.Forms.TextBox();
            this.txtPatientEmail = new System.Windows.Forms.TextBox();
            this.txtPatientID = new System.Windows.Forms.TextBox();
            this.txtPatientLastName = new System.Windows.Forms.TextBox();
            this.txtPatientFirstName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.grbAddPatient.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHospital
            // 
            this.lblHospital.AutoSize = true;
            this.lblHospital.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHospital.Location = new System.Drawing.Point(20, 18);
            this.lblHospital.Name = "lblHospital";
            this.lblHospital.Size = new System.Drawing.Size(272, 22);
            this.lblHospital.TabIndex = 31;
            this.lblHospital.Text = "Hospital Admission Software";
            // 
            // dgv
            // 
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(449, 415);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(392, 95);
            this.dgv.TabIndex = 36;
            // 
            // lstDisplay
            // 
            this.lstDisplay.FormattingEnabled = true;
            this.lstDisplay.Location = new System.Drawing.Point(27, 453);
            this.lstDisplay.Name = "lstDisplay";
            this.lstDisplay.Size = new System.Drawing.Size(394, 56);
            this.lstDisplay.TabIndex = 35;
            // 
            // lblDisplay
            // 
            this.lblDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDisplay.Location = new System.Drawing.Point(135, 414);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(286, 23);
            this.lblDisplay.TabIndex = 34;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemarks.Location = new System.Drawing.Point(38, 414);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(62, 20);
            this.lblRemarks.TabIndex = 33;
            this.lblRemarks.Text = "Remarks";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(167, 293);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(108, 29);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Search Staff";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.btnDeleteStaff);
            this.groupBox2.Controls.Add(this.txtSearchStaffID);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnStaffNames);
            this.groupBox2.Controls.Add(this.btnStaffContact);
            this.groupBox2.Controls.Add(this.btnShowStaff);
            this.groupBox2.Controls.Add(this.btnAddStaff);
            this.groupBox2.Controls.Add(this.dtpStaffDOB);
            this.groupBox2.Controls.Add(this.txtStaffPhone);
            this.groupBox2.Controls.Add(this.txtStaffEmail);
            this.groupBox2.Controls.Add(this.txtStaffID);
            this.groupBox2.Controls.Add(this.txtStaffLastName);
            this.groupBox2.Controls.Add(this.txtStaffFirstName);
            this.groupBox2.Controls.Add(this.cmbStaffTitle);
            this.groupBox2.Controls.Add(this.lblStaffPhone);
            this.groupBox2.Controls.Add(this.lblStaffEmail);
            this.groupBox2.Controls.Add(this.lblStaffID);
            this.groupBox2.Controls.Add(this.lblStaffDOB);
            this.groupBox2.Controls.Add(this.lblStaffTitle);
            this.groupBox2.Controls.Add(this.lblStaffLastName);
            this.groupBox2.Controls.Add(this.lblStaffFirstName);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(449, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(392, 352);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Add Staff";
            // 
            // btnDeleteStaff
            // 
            this.btnDeleteStaff.Location = new System.Drawing.Point(281, 293);
            this.btnDeleteStaff.Name = "btnDeleteStaff";
            this.btnDeleteStaff.Size = new System.Drawing.Size(108, 29);
            this.btnDeleteStaff.TabIndex = 20;
            this.btnDeleteStaff.Text = "Delete Staff";
            this.btnDeleteStaff.UseVisualStyleBackColor = true;
            this.btnDeleteStaff.Click += new System.EventHandler(this.btnDeleteStaff_Click);
            // 
            // txtSearchStaffID
            // 
            this.txtSearchStaffID.Location = new System.Drawing.Point(78, 297);
            this.txtSearchStaffID.Name = "txtSearchStaffID";
            this.txtSearchStaffID.Size = new System.Drawing.Size(80, 23);
            this.txtSearchStaffID.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "ID";
            // 
            // btnStaffNames
            // 
            this.btnStaffNames.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffNames.Location = new System.Drawing.Point(277, 136);
            this.btnStaffNames.Name = "btnStaffNames";
            this.btnStaffNames.Size = new System.Drawing.Size(108, 29);
            this.btnStaffNames.TabIndex = 17;
            this.btnStaffNames.Text = "Staff Names";
            this.btnStaffNames.UseVisualStyleBackColor = true;
            this.btnStaffNames.Click += new System.EventHandler(this.btnStaffNames_Click);
            // 
            // btnStaffContact
            // 
            this.btnStaffContact.Location = new System.Drawing.Point(277, 177);
            this.btnStaffContact.Name = "btnStaffContact";
            this.btnStaffContact.Size = new System.Drawing.Size(108, 29);
            this.btnStaffContact.TabIndex = 16;
            this.btnStaffContact.Text = "Contact";
            this.btnStaffContact.UseVisualStyleBackColor = true;
            this.btnStaffContact.Click += new System.EventHandler(this.btnStaffContact_Click);
            // 
            // btnShowStaff
            // 
            this.btnShowStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowStaff.Location = new System.Drawing.Point(277, 98);
            this.btnShowStaff.Name = "btnShowStaff";
            this.btnShowStaff.Size = new System.Drawing.Size(108, 29);
            this.btnShowStaff.TabIndex = 15;
            this.btnShowStaff.Text = "Show Staff";
            this.btnShowStaff.UseVisualStyleBackColor = true;
            this.btnShowStaff.Click += new System.EventHandler(this.btnShowStaff_Click);
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStaff.Location = new System.Drawing.Point(277, 57);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Size = new System.Drawing.Size(108, 29);
            this.btnAddStaff.TabIndex = 14;
            this.btnAddStaff.Text = "Add Staff";
            this.btnAddStaff.UseVisualStyleBackColor = true;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // dtpStaffDOB
            // 
            this.dtpStaffDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStaffDOB.Location = new System.Drawing.Point(138, 164);
            this.dtpStaffDOB.Name = "dtpStaffDOB";
            this.dtpStaffDOB.Size = new System.Drawing.Size(100, 23);
            this.dtpStaffDOB.TabIndex = 13;
            // 
            // txtStaffPhone
            // 
            this.txtStaffPhone.Location = new System.Drawing.Point(138, 230);
            this.txtStaffPhone.Name = "txtStaffPhone";
            this.txtStaffPhone.Size = new System.Drawing.Size(100, 23);
            this.txtStaffPhone.TabIndex = 12;
            // 
            // txtStaffEmail
            // 
            this.txtStaffEmail.Location = new System.Drawing.Point(138, 198);
            this.txtStaffEmail.Name = "txtStaffEmail";
            this.txtStaffEmail.Size = new System.Drawing.Size(100, 23);
            this.txtStaffEmail.TabIndex = 11;
            // 
            // txtStaffID
            // 
            this.txtStaffID.Location = new System.Drawing.Point(138, 34);
            this.txtStaffID.Name = "txtStaffID";
            this.txtStaffID.Size = new System.Drawing.Size(100, 23);
            this.txtStaffID.TabIndex = 10;
            // 
            // txtStaffLastName
            // 
            this.txtStaffLastName.Location = new System.Drawing.Point(138, 131);
            this.txtStaffLastName.Name = "txtStaffLastName";
            this.txtStaffLastName.Size = new System.Drawing.Size(100, 23);
            this.txtStaffLastName.TabIndex = 9;
            // 
            // txtStaffFirstName
            // 
            this.txtStaffFirstName.Location = new System.Drawing.Point(138, 98);
            this.txtStaffFirstName.Name = "txtStaffFirstName";
            this.txtStaffFirstName.Size = new System.Drawing.Size(100, 23);
            this.txtStaffFirstName.TabIndex = 8;
            // 
            // cmbStaffTitle
            // 
            this.cmbStaffTitle.FormattingEnabled = true;
            this.cmbStaffTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Ms ",
            "Miss"});
            this.cmbStaffTitle.Location = new System.Drawing.Point(138, 66);
            this.cmbStaffTitle.Name = "cmbStaffTitle";
            this.cmbStaffTitle.Size = new System.Drawing.Size(100, 24);
            this.cmbStaffTitle.TabIndex = 7;
            // 
            // lblStaffPhone
            // 
            this.lblStaffPhone.AutoSize = true;
            this.lblStaffPhone.Location = new System.Drawing.Point(42, 233);
            this.lblStaffPhone.Name = "lblStaffPhone";
            this.lblStaffPhone.Size = new System.Drawing.Size(49, 16);
            this.lblStaffPhone.TabIndex = 6;
            this.lblStaffPhone.Text = "Phone";
            // 
            // lblStaffEmail
            // 
            this.lblStaffEmail.AutoSize = true;
            this.lblStaffEmail.Location = new System.Drawing.Point(42, 201);
            this.lblStaffEmail.Name = "lblStaffEmail";
            this.lblStaffEmail.Size = new System.Drawing.Size(42, 16);
            this.lblStaffEmail.TabIndex = 5;
            this.lblStaffEmail.Text = "Email";
            // 
            // lblStaffID
            // 
            this.lblStaffID.AutoSize = true;
            this.lblStaffID.Location = new System.Drawing.Point(42, 37);
            this.lblStaffID.Name = "lblStaffID";
            this.lblStaffID.Size = new System.Drawing.Size(54, 16);
            this.lblStaffID.TabIndex = 4;
            this.lblStaffID.Text = "Staff ID";
            // 
            // lblStaffDOB
            // 
            this.lblStaffDOB.AutoSize = true;
            this.lblStaffDOB.Location = new System.Drawing.Point(42, 169);
            this.lblStaffDOB.Name = "lblStaffDOB";
            this.lblStaffDOB.Size = new System.Drawing.Size(38, 16);
            this.lblStaffDOB.TabIndex = 3;
            this.lblStaffDOB.Text = "DOB";
            // 
            // lblStaffTitle
            // 
            this.lblStaffTitle.AutoSize = true;
            this.lblStaffTitle.Location = new System.Drawing.Point(42, 69);
            this.lblStaffTitle.Name = "lblStaffTitle";
            this.lblStaffTitle.Size = new System.Drawing.Size(34, 16);
            this.lblStaffTitle.TabIndex = 2;
            this.lblStaffTitle.Text = "Title";
            // 
            // lblStaffLastName
            // 
            this.lblStaffLastName.AutoSize = true;
            this.lblStaffLastName.Location = new System.Drawing.Point(42, 134);
            this.lblStaffLastName.Name = "lblStaffLastName";
            this.lblStaffLastName.Size = new System.Drawing.Size(75, 16);
            this.lblStaffLastName.TabIndex = 1;
            this.lblStaffLastName.Text = "Last Name";
            // 
            // lblStaffFirstName
            // 
            this.lblStaffFirstName.AutoSize = true;
            this.lblStaffFirstName.Location = new System.Drawing.Point(42, 101);
            this.lblStaffFirstName.Name = "lblStaffFirstName";
            this.lblStaffFirstName.Size = new System.Drawing.Size(76, 16);
            this.lblStaffFirstName.TabIndex = 0;
            this.lblStaffFirstName.Text = "First Name";
            // 
            // cmbPatientTitle
            // 
            this.cmbPatientTitle.FormattingEnabled = true;
            this.cmbPatientTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Ms ",
            "Miss"});
            this.cmbPatientTitle.Location = new System.Drawing.Point(138, 66);
            this.cmbPatientTitle.Name = "cmbPatientTitle";
            this.cmbPatientTitle.Size = new System.Drawing.Size(100, 24);
            this.cmbPatientTitle.TabIndex = 7;
            // 
            // lblPatientPhone
            // 
            this.lblPatientPhone.AutoSize = true;
            this.lblPatientPhone.Location = new System.Drawing.Point(42, 233);
            this.lblPatientPhone.Name = "lblPatientPhone";
            this.lblPatientPhone.Size = new System.Drawing.Size(49, 16);
            this.lblPatientPhone.TabIndex = 6;
            this.lblPatientPhone.Text = "Phone";
            // 
            // lblPatientEmail
            // 
            this.lblPatientEmail.AutoSize = true;
            this.lblPatientEmail.Location = new System.Drawing.Point(42, 201);
            this.lblPatientEmail.Name = "lblPatientEmail";
            this.lblPatientEmail.Size = new System.Drawing.Size(42, 16);
            this.lblPatientEmail.TabIndex = 5;
            this.lblPatientEmail.Text = "Email";
            // 
            // lblPatientID
            // 
            this.lblPatientID.AutoSize = true;
            this.lblPatientID.Location = new System.Drawing.Point(42, 37);
            this.lblPatientID.Name = "lblPatientID";
            this.lblPatientID.Size = new System.Drawing.Size(69, 16);
            this.lblPatientID.TabIndex = 4;
            this.lblPatientID.Text = "Patient ID";
            // 
            // lblPatientDOB
            // 
            this.lblPatientDOB.AutoSize = true;
            this.lblPatientDOB.Location = new System.Drawing.Point(42, 169);
            this.lblPatientDOB.Name = "lblPatientDOB";
            this.lblPatientDOB.Size = new System.Drawing.Size(38, 16);
            this.lblPatientDOB.TabIndex = 3;
            this.lblPatientDOB.Text = "DOB";
            // 
            // lblPatientTitle
            // 
            this.lblPatientTitle.AutoSize = true;
            this.lblPatientTitle.Location = new System.Drawing.Point(42, 69);
            this.lblPatientTitle.Name = "lblPatientTitle";
            this.lblPatientTitle.Size = new System.Drawing.Size(34, 16);
            this.lblPatientTitle.TabIndex = 2;
            this.lblPatientTitle.Text = "Title";
            // 
            // lblPatientLastName
            // 
            this.lblPatientLastName.AutoSize = true;
            this.lblPatientLastName.Location = new System.Drawing.Point(42, 134);
            this.lblPatientLastName.Name = "lblPatientLastName";
            this.lblPatientLastName.Size = new System.Drawing.Size(75, 16);
            this.lblPatientLastName.TabIndex = 1;
            this.lblPatientLastName.Text = "Last Name";
            // 
            // lblPatientFirstName
            // 
            this.lblPatientFirstName.AutoSize = true;
            this.lblPatientFirstName.Location = new System.Drawing.Point(42, 101);
            this.lblPatientFirstName.Name = "lblPatientFirstName";
            this.lblPatientFirstName.Size = new System.Drawing.Size(76, 16);
            this.lblPatientFirstName.TabIndex = 0;
            this.lblPatientFirstName.Text = "First Name";
            // 
            // grbAddPatient
            // 
            this.grbAddPatient.Controls.Add(this.btnSearchPatient);
            this.grbAddPatient.Controls.Add(this.btnDeletePatient);
            this.grbAddPatient.Controls.Add(this.txtShearchPatientID);
            this.grbAddPatient.Controls.Add(this.lblShearchPatient);
            this.grbAddPatient.Controls.Add(this.btnPatientNames);
            this.grbAddPatient.Controls.Add(this.btnPatientContact);
            this.grbAddPatient.Controls.Add(this.btnShowPatient);
            this.grbAddPatient.Controls.Add(this.btnAddPatient);
            this.grbAddPatient.Controls.Add(this.dtpPatientDOB);
            this.grbAddPatient.Controls.Add(this.txtPatientPhone);
            this.grbAddPatient.Controls.Add(this.txtPatientEmail);
            this.grbAddPatient.Controls.Add(this.txtPatientID);
            this.grbAddPatient.Controls.Add(this.txtPatientLastName);
            this.grbAddPatient.Controls.Add(this.txtPatientFirstName);
            this.grbAddPatient.Controls.Add(this.cmbPatientTitle);
            this.grbAddPatient.Controls.Add(this.lblPatientPhone);
            this.grbAddPatient.Controls.Add(this.lblPatientEmail);
            this.grbAddPatient.Controls.Add(this.lblPatientID);
            this.grbAddPatient.Controls.Add(this.lblPatientDOB);
            this.grbAddPatient.Controls.Add(this.lblPatientTitle);
            this.grbAddPatient.Controls.Add(this.lblPatientLastName);
            this.grbAddPatient.Controls.Add(this.lblPatientFirstName);
            this.grbAddPatient.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbAddPatient.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.grbAddPatient.Location = new System.Drawing.Point(27, 56);
            this.grbAddPatient.Name = "grbAddPatient";
            this.grbAddPatient.Size = new System.Drawing.Size(394, 355);
            this.grbAddPatient.TabIndex = 30;
            this.grbAddPatient.TabStop = false;
            this.grbAddPatient.Text = "Add Patient";
            // 
            // btnSearchPatient
            // 
            this.btnSearchPatient.Location = new System.Drawing.Point(159, 293);
            this.btnSearchPatient.Name = "btnSearchPatient";
            this.btnSearchPatient.Size = new System.Drawing.Size(114, 29);
            this.btnSearchPatient.TabIndex = 21;
            this.btnSearchPatient.Text = "Search Patient";
            this.btnSearchPatient.UseVisualStyleBackColor = true;
            this.btnSearchPatient.Click += new System.EventHandler(this.btnSearchPatient_Click);
            // 
            // btnDeletePatient
            // 
            this.btnDeletePatient.Location = new System.Drawing.Point(279, 293);
            this.btnDeletePatient.Name = "btnDeletePatient";
            this.btnDeletePatient.Size = new System.Drawing.Size(108, 29);
            this.btnDeletePatient.TabIndex = 20;
            this.btnDeletePatient.Text = "Delete Patient";
            this.btnDeletePatient.UseVisualStyleBackColor = true;
            this.btnDeletePatient.Click += new System.EventHandler(this.btnDeletePatient_Click);
            // 
            // txtShearchPatientID
            // 
            this.txtShearchPatientID.Location = new System.Drawing.Point(73, 296);
            this.txtShearchPatientID.Name = "txtShearchPatientID";
            this.txtShearchPatientID.Size = new System.Drawing.Size(80, 23);
            this.txtShearchPatientID.TabIndex = 19;
            // 
            // lblShearchPatient
            // 
            this.lblShearchPatient.AutoSize = true;
            this.lblShearchPatient.Location = new System.Drawing.Point(46, 299);
            this.lblShearchPatient.Name = "lblShearchPatient";
            this.lblShearchPatient.Size = new System.Drawing.Size(21, 16);
            this.lblShearchPatient.TabIndex = 18;
            this.lblShearchPatient.Text = "ID";
            // 
            // btnPatientNames
            // 
            this.btnPatientNames.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPatientNames.Location = new System.Drawing.Point(277, 136);
            this.btnPatientNames.Name = "btnPatientNames";
            this.btnPatientNames.Size = new System.Drawing.Size(108, 29);
            this.btnPatientNames.TabIndex = 17;
            this.btnPatientNames.Text = "Patient Names";
            this.btnPatientNames.UseVisualStyleBackColor = true;
            this.btnPatientNames.Click += new System.EventHandler(this.btnPatientNames_Click);
            // 
            // btnPatientContact
            // 
            this.btnPatientContact.Location = new System.Drawing.Point(277, 177);
            this.btnPatientContact.Name = "btnPatientContact";
            this.btnPatientContact.Size = new System.Drawing.Size(108, 29);
            this.btnPatientContact.TabIndex = 16;
            this.btnPatientContact.Text = "Contact";
            this.btnPatientContact.UseVisualStyleBackColor = true;
            this.btnPatientContact.Click += new System.EventHandler(this.btnPatientContact_Click);
            // 
            // btnShowPatient
            // 
            this.btnShowPatient.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowPatient.Location = new System.Drawing.Point(277, 98);
            this.btnShowPatient.Name = "btnShowPatient";
            this.btnShowPatient.Size = new System.Drawing.Size(108, 29);
            this.btnShowPatient.TabIndex = 15;
            this.btnShowPatient.Text = "Show Patient";
            this.btnShowPatient.UseVisualStyleBackColor = true;
            this.btnShowPatient.Click += new System.EventHandler(this.btnShowPatient_Click);
            // 
            // btnAddPatient
            // 
            this.btnAddPatient.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPatient.Location = new System.Drawing.Point(277, 57);
            this.btnAddPatient.Name = "btnAddPatient";
            this.btnAddPatient.Size = new System.Drawing.Size(108, 29);
            this.btnAddPatient.TabIndex = 14;
            this.btnAddPatient.Text = "Add Patient";
            this.btnAddPatient.UseVisualStyleBackColor = true;
            this.btnAddPatient.Click += new System.EventHandler(this.btnAddPatient_Click);
            // 
            // dtpPatientDOB
            // 
            this.dtpPatientDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPatientDOB.Location = new System.Drawing.Point(138, 164);
            this.dtpPatientDOB.Name = "dtpPatientDOB";
            this.dtpPatientDOB.Size = new System.Drawing.Size(100, 23);
            this.dtpPatientDOB.TabIndex = 13;
            // 
            // txtPatientPhone
            // 
            this.txtPatientPhone.Location = new System.Drawing.Point(138, 230);
            this.txtPatientPhone.Name = "txtPatientPhone";
            this.txtPatientPhone.Size = new System.Drawing.Size(100, 23);
            this.txtPatientPhone.TabIndex = 12;
            // 
            // txtPatientEmail
            // 
            this.txtPatientEmail.Location = new System.Drawing.Point(138, 198);
            this.txtPatientEmail.Name = "txtPatientEmail";
            this.txtPatientEmail.Size = new System.Drawing.Size(100, 23);
            this.txtPatientEmail.TabIndex = 11;
            // 
            // txtPatientID
            // 
            this.txtPatientID.Location = new System.Drawing.Point(138, 34);
            this.txtPatientID.Name = "txtPatientID";
            this.txtPatientID.Size = new System.Drawing.Size(100, 23);
            this.txtPatientID.TabIndex = 10;
            // 
            // txtPatientLastName
            // 
            this.txtPatientLastName.Location = new System.Drawing.Point(138, 131);
            this.txtPatientLastName.Name = "txtPatientLastName";
            this.txtPatientLastName.Size = new System.Drawing.Size(100, 23);
            this.txtPatientLastName.TabIndex = 9;
            // 
            // txtPatientFirstName
            // 
            this.txtPatientFirstName.Location = new System.Drawing.Point(138, 98);
            this.txtPatientFirstName.Name = "txtPatientFirstName";
            this.txtPatientFirstName.Size = new System.Drawing.Size(100, 23);
            this.txtPatientFirstName.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 542);
            this.Controls.Add(this.lblHospital);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.lstDisplay);
            this.Controls.Add(this.lblDisplay);
            this.Controls.Add(this.lblRemarks);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grbAddPatient);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grbAddPatient.ResumeLayout(false);
            this.grbAddPatient.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHospital;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.ListBox lstDisplay;
        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeleteStaff;
        private System.Windows.Forms.TextBox txtSearchStaffID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStaffNames;
        private System.Windows.Forms.Button btnStaffContact;
        private System.Windows.Forms.Button btnShowStaff;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.DateTimePicker dtpStaffDOB;
        private System.Windows.Forms.TextBox txtStaffPhone;
        private System.Windows.Forms.TextBox txtStaffEmail;
        private System.Windows.Forms.TextBox txtStaffID;
        private System.Windows.Forms.TextBox txtStaffLastName;
        private System.Windows.Forms.TextBox txtStaffFirstName;
        private System.Windows.Forms.ComboBox cmbStaffTitle;
        private System.Windows.Forms.Label lblStaffPhone;
        private System.Windows.Forms.Label lblStaffEmail;
        private System.Windows.Forms.Label lblStaffID;
        private System.Windows.Forms.Label lblStaffDOB;
        private System.Windows.Forms.Label lblStaffTitle;
        private System.Windows.Forms.Label lblStaffLastName;
        private System.Windows.Forms.Label lblStaffFirstName;
        private System.Windows.Forms.ComboBox cmbPatientTitle;
        private System.Windows.Forms.Label lblPatientPhone;
        private System.Windows.Forms.Label lblPatientEmail;
        private System.Windows.Forms.Label lblPatientID;
        private System.Windows.Forms.Label lblPatientDOB;
        private System.Windows.Forms.Label lblPatientTitle;
        private System.Windows.Forms.Label lblPatientLastName;
        private System.Windows.Forms.Label lblPatientFirstName;
        private System.Windows.Forms.GroupBox grbAddPatient;
        private System.Windows.Forms.Button btnSearchPatient;
        private System.Windows.Forms.Button btnDeletePatient;
        private System.Windows.Forms.TextBox txtShearchPatientID;
        private System.Windows.Forms.Label lblShearchPatient;
        private System.Windows.Forms.Button btnPatientNames;
        private System.Windows.Forms.Button btnPatientContact;
        private System.Windows.Forms.Button btnShowPatient;
        private System.Windows.Forms.Button btnAddPatient;
        private System.Windows.Forms.DateTimePicker dtpPatientDOB;
        private System.Windows.Forms.TextBox txtPatientPhone;
        private System.Windows.Forms.TextBox txtPatientEmail;
        private System.Windows.Forms.TextBox txtPatientID;
        private System.Windows.Forms.TextBox txtPatientLastName;
        private System.Windows.Forms.TextBox txtPatientFirstName;

    }
}

