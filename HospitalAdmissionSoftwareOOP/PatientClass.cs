﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalAdmissionSoftwareOOP
{
    public class Patient : Person
    {
        public int PatientID { get; set; }

        public string GetAllPatients()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(PatientID);
            sb.Append(" ");
            sb.Append(base.GetAll());

            return sb.ToString();
        }
    }
}
