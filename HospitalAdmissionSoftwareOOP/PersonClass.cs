﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalAdmissionSoftwareOOP
{
    public class Person
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }


        public string GetAll()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Title);
            sb.Append(" ");
            sb.Append(FirstName);
            sb.Append(" ");
            sb.Append(Surname);
            sb.Append(" ");
            sb.Append(DOB);
            sb.Append(" ");
            sb.Append(Email);
            sb.Append(" ");
            sb.Append(Phone);

            return sb.ToString();
        }


        public override string ToString()
        {
            return string.Format(FirstName + " " + Surname);
        }

        public string GetContacts()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(FirstName);
            sb.Append(" ");
            sb.Append(Email);
            sb.Append(" ");
            sb.Append(Phone);

            return sb.ToString();
        }
    }
}
