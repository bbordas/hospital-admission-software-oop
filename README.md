# Short Description for a Hospital admission Software #


First of all, I tried to find out how would the software work. Beside the given instructions what else can be an important function to include. Secondly I made the GUI design, which is not too complicated and easy to use. 
I created 3 public classes, a Patient Class with Patient ID and a Staff Class with Staff ID inherited from a Person Class which includes all the personal data except ID-s. (title, first name, last name, date of birth, email, phone number)

I have created a People list which includes 6 methods (add staff/patient, delete staff/patient, and search staff/patient). One button belongs to each method. The user can search and delete persons by their ID-s. If a person is deleted from the database, then the data grid view loads the updated data immediately.


```
#!c#

int removestaffid = int.Parse(txtSearchStaffID.Text);
lblDisplay.Text = p.DeleteStaff(removestaffid);
dgv.DataSource = p.staff;
```


Beside the main functions of the buttons, I also added error handling functions where it was necessary. (e.g. try/catch for int parsing)

```
#!c#

try
            {
                foreach (Patient per in p.patient)
                {
                    int search = int.Parse(txtShearchPatientID.Text);
                    lblDisplay.Text = p.DisplayMatchPatient(search);
                }
            }
catch
            {
                MessageBox.Show("Please enter a valid ID");
            }
```


I wrote different information messages for the users which can be shown in the display label after different methods are completed. 

```
#!c#

return "ID Exists in the list"; 
return title +" "+firstname +" " + lastname + " added to the Patient list";
return "No match found";
```


I tested the program manually throughout the whole process, I tried all the possible outcomes and fixed the errors.

Finally, I created two test classes based on the project. Test one is a unit test for patients and test two is a unit test for staff. Both can test the main functions such as add, delete and search patients or staff.
      
## Technical Merit: ##

### I have used: ### 

* StringBuilder
* BindingSource
* override ToString
* try {} catch{}
* DialogResult and MessageBoxButtons
* MessageBox.Show